Rails.application.routes.draw do
  get 'welcome/index'
  
  resources :quotes

  root 'quotes#index'
end
